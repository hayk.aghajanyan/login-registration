const initialState = {
    login: "",
    password: "",
    email: "",
    phoneNumber: "",
}

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_LOGIN':
            return {
                ...state,
                login: action.payload
            }
        case 'SET_PASSWORD':
            return {
                ...state,
                password: action.payload
            }
        case 'SET_EMAIL':
            return {
                ...state,
                email: action.payload
            }
        case 'SET_PHONE_NUMBER':
            return {
                ...state,
                phoneNumber: action.payload
            }
        default:
            return state
    }

}

export default loginReducer;