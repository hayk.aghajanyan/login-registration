import React from "react";
import {Typography} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
    errorMSG: {
        textAlign: "center",
        margin: "50px auto"
    }
})

const ErrorPage = () => {
    const classes = useStyles();

    return (
        <Typography variant="h2" color="secondary" className={classes.errorMSG}>
            404 Error: There is no such directory
        </Typography>
    )
}

export default ErrorPage