import React from 'react';
import ReactDOM from 'react-dom';
import './assets/login.css';
import './index.css';
import App from './App';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import store from "./redux";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { theme } from "./components/theme";

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <App />
            </ThemeProvider>
        </Provider>
    </BrowserRouter>,
  document.getElementById('root')
);