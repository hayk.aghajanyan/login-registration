import React from 'react';
import Header from "./components/Header";
import {Route, Switch} from "react-router-dom";
import Register from "./components/Register";
import Login from "./components/Login";
import ErrorPage from "./components/ErrorPage";
import Home from "./components/Home";

function App() {
  return (
    <div>
      <Header/>
      <Switch>
        <Route path="/" component={Home} exact/>
        <Route path="/registration" component={Register} exact/>
        <Route path="/login" component={Login} exact/>
        <Route component={ErrorPage} />
      </Switch>
    </div>
  );
}

export default App;
