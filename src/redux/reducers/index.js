import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import signInReducer from "./signInReducer";

const rootReducer = combineReducers({
    loginReducer,
    signInReducer
})

export default rootReducer;