export const loginSelector = ({loginReducer}) => {
    return {
        login: loginReducer.login,
        phoneNumber: loginReducer.phoneNumber,
        email: loginReducer.email,
    }
}