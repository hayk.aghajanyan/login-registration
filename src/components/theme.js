import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#2f9774',
            light: '#F5A946',
            dark: '#DA800B',
        },
        secondary: {
            main: '#50355e',
            dark: '#c45d37',
        },
    },
    typography: {
        fontFamily: [
            'Helvetica Neue',
            'Ubuntu',
            'Futura PT',
            'sans-serif'
        ]
    }
})