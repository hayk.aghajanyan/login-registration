import React from 'react';
import logo from '../../assets/dgs_logo.svg'
import { NavLink } from "react-router-dom";
import { AppBar, IconButton, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useSelector, useDispatch } from "react-redux";
import { clearUser } from "../../redux/actions/signIn";
import { toast } from "react-toastify";

const useStyles = makeStyles({
    headerStyle: {
        display: "flex",
        justifyContent: "space-between"
    },
    navBar: {
        display: "flex",
    },
    TypographyNavLink: {
        marginRight: "25px",
    },
    navLink: {
        textDecoration: "none",
        textTransform: "uppercase",
        margin: "0 auto",
        width: "100%",
        color: '#ff1c4d',
    },
    active: {
        color: "#fff",
    }
})

const Header = () => {
    const loggedIn = useSelector(({signInReducer}) => signInReducer.loggedIn);
    const dispatch = useDispatch();
    const classes = useStyles();

    toast.configure();

    const logoutHandler = () => {
        dispatch(clearUser())
    }

    const authHandler = () => {
        !loggedIn && toast.warn("Please, log in before", {position: "top-right", autoClose: 2500})
    }

    return (
        <AppBar color="secondary" position="sticky">
            <Toolbar className={classes.headerStyle}>
                <IconButton>
                    <NavLink to="/" exact>
                        <img src={logo} alt="#"/>
                    </NavLink>
                </IconButton>
                <nav className={classes.navBar}>
                    <Typography  className={classes.TypographyNavLink}>
                        <NavLink className={classes.navLink}
                                 activeClassName={classes.active}
                                 to="/"
                                 onClick={authHandler}
                                 exact
                        >Home</NavLink>
                    </Typography>
                    {!loggedIn &&
                    <Typography className={classes.TypographyNavLink}>
                        <NavLink className={classes.navLink}
                                 activeClassName={classes.active}
                                 to="/registration"
                        >Registration</NavLink>
                    </Typography>
                    }
                    {!loggedIn &&
                    <Typography  className={classes.TypographyNavLink}>
                        <NavLink className={classes.navLink}
                                 activeClassName={classes.active}
                                 to="/login"
                        >Login</NavLink>
                    </Typography>
                    }
                    {
                        loggedIn &&
                        <Typography  className={classes.TypographyNavLink}>
                            <NavLink className={classes.navLink}
                                     activeClassName={classes.active}
                                     to="/login"
                                     onClick={logoutHandler}
                            >Log out</NavLink>
                        </Typography>
                    }
                </nav>
            </Toolbar>
        </AppBar>
    );
}

export default Header;
