import {emailHandler, loginHandler, passwordHandler, phoneNumberHandler} from "./register";
import {batch} from "react-redux";

export const signIn = () => ({
    type: 'SET_LOGGED_IN'
})

export const signOut = () => ({
    type: 'SET_LOGGED_OUT'
})

export const clearUser = () => (dispatch) => {
    batch(() => {
        dispatch(signOut());
        dispatch(loginHandler(""));
        dispatch(passwordHandler(""));
        dispatch(emailHandler(""));
        dispatch(phoneNumberHandler(""));
    })
}