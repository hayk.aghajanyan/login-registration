export const loginHandler = (payload) => ({
    type: 'SET_LOGIN',
    payload
})

export const passwordHandler = (payload) => ({
    type: 'SET_PASSWORD',
    payload
})

export const emailHandler = (payload) => ({
    type: 'SET_EMAIL',
    payload
})

export const phoneNumberHandler = (payload) => ({
    type: 'SET_PHONE_NUMBER',
    payload
})