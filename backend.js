let express = require('express');
let jwt = require('jsonwebtoken');
const app = express();

app.get('/login', function (req, res) {
    const user = {id: 3};
    const token = jwt.sign({ user }, 'secret_key');
    res.json({
        token: token
    });
})

app.get('/protected/users', ensureToken, function (req, res) {
    jwt.verify(req.token, 'secret_key', function (err, data) {
      if(err) {
          res.sendStatus(403)
      } else {
          res.json({
              text: "Protected way",
              data: data
          });
      }
    });
})

function ensureToken(req, res, next) {
    const bearerHeader = req.headers["authorization"];
    if(typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        req.token = bearer[1];
        next();
    } else {
        res.sendStatus(403);
    }
}





app.listen(3001, function () {
    console.log('App listening on port 3001')
});

