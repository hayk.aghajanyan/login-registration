import React, { useEffect, useRef, useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { Typography } from "@material-ui/core";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css"
import { useStyles } from "../Login"

const arr = []


const Register = () => {
    const [clickedEye, setClickedEye] = useState(false),
          [login, setLogin] = useState(""),
          [password, setPassword] = useState(""),
          [phoneNumber, setPhoneNumber] = useState(""),
          [email, setEmail] = useState("");

    const formRef = useRef(),
          loginRef = useRef(),
          passwordRef = useRef(),
          phoneRef = useRef(),
          emailRef = useRef();

    const classes = useStyles();
    toast.configure();

    const eyeChange = () => {
        setClickedEye(prev => !prev);
    }

    const addClassname = (node) => {
        node.current.classList.add("has-val")
    }

    const removeClassname = (node) => {
        node.current.classList.remove("has-val")
    }

    useEffect(() => {
        if(login.trim() !== "") {
            addClassname(loginRef)
        } else {
            removeClassname(loginRef)
        }
        if(password.trim() !== "") {
            addClassname(passwordRef)
        } else {
            removeClassname(passwordRef)
        }
        if(phoneNumber.trim() !== "") {
            addClassname(phoneRef)
        } else {
            removeClassname(phoneRef)
        }
        if(email.trim() !== "") {
            addClassname(emailRef)
        } else {
            removeClassname(emailRef)
        }
    }, [login, password, phoneNumber, email])



    const formHandler = (e) => {
        e.preventDefault();
        if(!sessionStorage.getItem(`${login}`)) {
            for (let i = 0; i < sessionStorage.length; i++) {
                arr.push(JSON.parse(sessionStorage.getItem(sessionStorage.key(i))).email !== email.slice())
            }
            if(arr.every((item) => item === true) === false) {
                arr.length = 0;
                setEmail("");
                toast.error("email is already taken", {position: "top-right", autoClose: 2500});
            } else {
                sessionStorage.setItem(`${login}`, JSON.stringify({
                    login,
                    password,
                    email,
                    phoneNumber,
                }))
                setLogin("");
                setPassword("");
                setPhoneNumber("");
                setEmail("");
                toast.success("You are registered", {position: "top-right", autoClose: 2500});
            }
        } else {
            toast.error("user already registered");
        }
        arr.length = 0;
    }

    const setFormValue = (e) => {
        switch (e.target.name) {
            case "login":
                setLogin(e.target.value)
                break
            case "password":
                setPassword(e.target.value)
                break
            case "phone":
                setPhoneNumber(e.target.value)
                break
            case "email":
                setEmail(e.target.value)
                break
            default:
                break
        }
    }

    return (
        <div className={classes.container}>
            <div className={classes.containerWrapper}>
                <div className="wrap-login100 m-20">
                    <form ref={formRef} className="login100-form validate-form">
					<Typography variant="h4" className="login100-form-title p-b-16">
						Create Account
					</Typography>
                        <span className="login100-form-title p-b-28">
					</span>

                        <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                            <input
                                className="input100"
                                type="text"
                                name="login"
                                ref={loginRef}
                                onChange={setFormValue}
                                value={login}
                            />
                            <span className="focus-input100" data-placeholder="Login"></span>
                        </div>

                        <div className="wrap-input100 validate-input" data-validate="Enter password">
                            <span className="btn-show-pass">
                                <FontAwesomeIcon onClick={eyeChange} icon={clickedEye ? faEye : faEyeSlash}/>
                            </span>
                            <input
                                className="input100"
                                type={clickedEye ? "text" : "password"}
                                name="password"
                                ref={passwordRef}
                                onChange={setFormValue}
                                value={password}
                            />
                            <span className="focus-input100" data-placeholder="Password"></span>
                        </div>

                        <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                            <input
                                className="input100"
                                type="phone"
                                name="phone"
                                ref={phoneRef}
                                onChange={setFormValue}
                                value={phoneNumber}
                            />
                            <span className="focus-input100" data-placeholder="Phone number"></span>
                        </div>

                        <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                            <input
                                className="input100"
                                type="text"
                                name="email"
                                ref={emailRef}
                                onChange={setFormValue}
                                value={email}
                            />
                            <span className="focus-input100" data-placeholder="Email"></span>
                        </div>

                        <div className="container-login100-form-btn">
                            <div className="wrap-login100-form-btn">
                                <div className="login100-form-bgbtn"></div>
                                <button onClick={formHandler} className="login100-form-btn">
                                    Sign Up
                                </button>
                            </div>
                        </div>

                        <div className="text-center p-t-60">
						<span className="txt1">
							Already has an account?
						</span>
                            <Link to="/login" className="no-decor">Sign In</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Register;
