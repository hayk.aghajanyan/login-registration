import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, batch } from "react-redux";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import { Link } from "react-router-dom";
import "../../assets/login.css"
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css"
import {signIn} from "../../redux/actions/signIn";
import {emailHandler, loginHandler, passwordHandler, phoneNumberHandler} from "../../redux/actions/register";

export const useStyles = makeStyles({
    container: {
        width: "100%",
        margin: "0 auto",
    },
    containerWrapper: {
        width: "100%",
        minHeight: "100vh",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        background: "#f2f2f2",
    },
    formWrapper: {
        width: 390,
        background: "#fff",
        borderRadius: 10,
        overflow: "hidden",
        padding: "77px 55px 33px 55px",
        boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.1)",
    },
})

const Login = ({history}) => {
    const [clickedEye, setClickedEye] = useState(false);
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const loginRef = useRef();
    const passwordRef = useRef();
    const dispatch = useDispatch()
    const classes = useStyles();

    toast.configure();
    const eyeChange = () => {
        setClickedEye(prev => !prev);
    }

    const addClassname = (node) => {
        node.current.classList.add("has-val")
    }

    const removeClassname = (node) => {
        node.current.classList.remove("has-val")
    }

    useEffect(() => {
        if(login.trim() !== "") {
            addClassname(loginRef)
        } else {
            removeClassname(loginRef)
        }
        if(password.trim() !== "") {
            addClassname(passwordRef)
        } else {
            removeClassname(passwordRef)
        }
    }, [login, password])

    const setFormValue = (e) => {
        switch (e.target.name) {
            case "login":
                setLogin(e.target.value)
                break
            case "password":
                setPassword(e.target.value)
                break
            default:
                break
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(sessionStorage.getItem(`${login}`)) {
            if(JSON.parse(sessionStorage.getItem(`${login}`)).password === password) {
                const { password, email, phoneNumber } = JSON.parse(sessionStorage.getItem(`${login}`))

                toast.success(`Welcome back, ${login}`, {position: "top-right", autoClose: 2500})

                batch(() => {
                    dispatch(signIn())
                    dispatch(loginHandler(login))
                    dispatch(passwordHandler(password))
                    dispatch(emailHandler(email))
                    dispatch(phoneNumberHandler(phoneNumber))
                })

                history.push("/")

            } else {
                toast.error("Wrong password", {position: "top-right", autoClose: 2500})
            }
        } else {
            toast.error("There is no such user, you can register first", {position: "top-right", autoClose: 3000})
        }
    }

    return (
        <div className={classes.container}>
            <div className={classes.containerWrapper}>
                <div className={classes.formWrapper}>
                    <form className="login100-form validate-form">
					<Typography variant="h4" className="login100-form-title p-b-16">
						Welcome
					</Typography>
                        <span className="login100-form-title p-b-48">
                        </span>

                        <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                            <input
                                ref={loginRef}
                                onChange={setFormValue}
                                value={login}
                                className="input100"
                                type="text"
                                name="login"
                            />
                                <span className="focus-input100" data-placeholder="Login"></span>
                        </div>

                        <div className="wrap-input100 validate-input" data-validate="Enter password">
                            <span className="btn-show-pass">
                                <FontAwesomeIcon onClick={eyeChange} icon={clickedEye ? faEye : faEyeSlash}/>
                            </span>
                            <input
                                ref={passwordRef}
                                onChange={setFormValue}
                                value={password}
                                className="input100"
                                type={clickedEye ? "text" : "password"}
                                name="password"
                            />
                            <span className="focus-input100" data-placeholder="Password"></span>
                        </div>

                        <div className="container-login100-form-btn">
                            <div className="wrap-login100-form-btn">
                                <div className="login100-form-bgbtn"></div>
                                <button className="login100-form-btn" onClick={handleSubmit}>
                                    Login
                                </button>
                            </div>
                        </div>

                        <div className="text-center p-t-60">
						<span className="txt1">
							Don’t have an account?
						</span>
                            <Link to="/registration" className="no-decor">Sign Up</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;
